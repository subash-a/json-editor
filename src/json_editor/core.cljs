(ns ^:figwheel-always json-editor.core
  (:require-macros [cljs.core.async.macros :refer [go]])
  (:require [om.core :as om :include-macros true]
            [om.dom :as dom :include-macros true]
            [cljs.core.async :refer [put! chan <!]]
            [clojure.string :as string]))

(enable-console-print!)

(println "Edits to this text should show up in your developer console.")

;; define your app data so that it doesn't get over-written on reload

(defonce app-state (atom {:text "Hello world!"}))

(defonce app-list (atom {:list ["Lion" "Tiger" "Baboon"]}))

;; (def stru (. js/JSON (parse "{\"hello\": \"world\"}")))
;; (println stru)

;; (om/root
;;   (fn [data owner]
;;     (reify om/IRender
;;       (render [_]
;;         (dom/h1 nil (:text data)))))
;;   app-state
;;   {:target (. js/document (getElementById "app0"))})

;; (defn striper [text bg]
;;   (let [syl #js {:backgroundColor bg}]
;;     (dom/li #js {:style syl} text)))


;; (om/root
;;  (fn [data owner]
;;    (reify om/IRender
;;      (render [_]
;;        (apply dom/ul nil
;;               (map striper (:list data) (cycle ["#eee" "#fff"]))))))
;;  app-list
;;  {:target (. js/document (getElementById "app1"))})

;; (swap! app-state assoc :text "Hello Shruthi Dear!")


(defonce app-contacts (atom {:contacts
                             [{:first "Ben" :last "Alamos" :email "benalamos@mos.com"}
                              {:first "Sub" :last "Bash" :email "subash@mos.com"}]}))

(defn middle-name [{:keys [middle middle-initial]}]
  (cond middle (str " " middle)
        middle-initial (str " " middle-initial ".")))

(defn display-name [{:keys [first last] :as contact}]
  (str last "," first (middle-name contact)))

(defn contact-view [contact owner]
  (reify om/IRenderState
    (render-state [this {:keys [delete]}]
      (dom/li nil
              (dom/span nil (display-name contact))
              (dom/button #js {:onClick (fn [e] (put! delete @contact))}"Delete")))))


(defn contacts-view [data owner]
  (reify
    om/IInitState
    (init-state [_]
      {:delete (chan)})
    om/IWillMount
    (will-mount [_]
      (let [delete (om/get-state owner :delete)]
        (go (loop []
              (let [contact (<! delete)]
                (om/transact! data :contacts
                              (fn [xs] (vec (remove #(= contact %) xs))))
                (recur))))))
    om/IRenderState
    (render-state [this {:keys [delete]}]
      (dom/div nil
               (dom/h2 nil "Contacts List"
                       (apply dom/ul nil
                              (om/build-all contact-view (:contacts data)
                                            {:init-state {:delete delete}})))))))
  ;; (reify om/IRender
  ;;   (render [this]
  ;;     (dom/div nil
  ;;              (dom/h2 nil "Contacts List"
  ;;                      (apply dom/ul nil
  ;;                             (om/build-all contact-view (:contacts data))))))))

(om/root contacts-view app-contacts {:target (. js/document (getElementById "app0"))})

(defonce node-list (atom {:values ["banana" "apple" "oranges" "peach"]}))

(defn leaf-node-view [data owner]
   (reify om/IRender
     (render [this] (dom/li nil data))))

(defn create-leaf-nodes [data]
  (om/build-all leaf-node-view (:values data)))

(defn node-view [data owner]
  (reify om/IRender
    (render [this]
      (dom/ul nil (create-leaf-nodes data)))))

;; (om/root node-view node-list {:target (. js/document (getElementById "app1"))})

(defn collapsible-view [data owner]
  (reify om/IRenderState
    (render-state [this state]
      (let [jon {:key (first data) :value (last data)}]
        (print data)
        (dom/li nil
               (dom/a #js {:style #js {:fontWeight "bold"} :onClick (fn [e] (om/set-state! owner :open (not (:open state))))} (:key jon))
               (dom/span nil " : ")
               (dom/span nil (if (:open state)
                                         (:value jon)
                                         "...")))))))

;; (om/root collapsible-view ["key" "value"] {:target (. js/document (getElementById "app1")) :init-state {:open true}})

(defn parse-string [str]
  (into [] (js->clj (.parse js/JSON str))))

(defn json-view [data owner]
  (reify om/IRenderState
    (render-state [this state]
      (apply dom/ul nil
             (om/build-all collapsible-view (parse-string (:data data)) {:init-state {:open true}})))))

(defonce blunder (atom {:data "{\"hello\":\"world\",\"big\":\"jovial\"}"}))

;; (om/root json-view blunder {:target (. js/document (getElementById "app1"))})

(defonce j (atom {:json {:data "{\"hello\":\"world\"}"}}))

(defn handle-input [owner]
  (-> (om/get-node owner "json-input")
    .-value))

(defn json-window [data owner]
  (reify om/IRenderState
    (render-state [this _]
      (dom/div nil
               (dom/textarea #js {:ref "json-input"} "{\"key\":\"value\"}")
               (dom/button #js {:onClick (fn [e] (om/transact! data (fn [d] (assoc d :json (assoc (:json d) :data (handle-input owner))))))} "Load")
               ;; (dom/div nil (:json data))
               (om/build json-view (:json data) nil)
               ))))

(om/root json-window j {:target (. js/document (getElementById "app1"))})


(defn on-js-reload []
  ;; optionally touch your app-state to force rerendering depending on
  ;; your application
  ;; (swap! app-state update-in [:__figwheel_counter] inc)
)
